/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisData;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;

/**
 *
 * @author SPerkins
 */
public class AnalysisDataRelational implements AnalysisData {
    private MzidRelationalManager manager = MzidRelationalManager.getInstance();
    private final int db_id;
    public AnalysisDataRelational(int db_id) {
        this.db_id = db_id;
    }

    @Override
    public List<SpectrumIdentificationList> getSpectrumIdentificationLists() {
        try {
            List<SpectrumIdentificationList> list = manager.getSpectrumIdentificationLists(db_id);
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AnalysisDataRelational.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public List<ProteinDetectionList> getProteinDetectionLists() {
        try {
            List<ProteinDetectionList> list = manager.getProteinDetectionLists(db_id);
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(AnalysisDataRelational.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
}

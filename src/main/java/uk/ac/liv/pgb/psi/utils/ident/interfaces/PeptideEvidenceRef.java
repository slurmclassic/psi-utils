
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

/**
 *
 * @author SPerkins
 */
public interface PeptideEvidenceRef {
    String getRef();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Fragmentation;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.IonType;



/**
 *
 * @author SPerkins
 */
public class FragmentationStandard implements Fragmentation {
    private uk.ac.ebi.jmzidml.model.mzidml.Fragmentation frag;
    
    public FragmentationStandard(uk.ac.ebi.jmzidml.model.mzidml.Fragmentation frag) {
        this.frag = frag;
    }

    @Override
    public Iterator<IonType> iterator() {
        return new Iterator<IonType>() {
            private Iterator<uk.ac.ebi.jmzidml.model.mzidml.IonType> localIterator = frag.getIonType().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public IonType next() {
                return new IonTypeStandard(localIterator.next());
            }
        };        
    }
}

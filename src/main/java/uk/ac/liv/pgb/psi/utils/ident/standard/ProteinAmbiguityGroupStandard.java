/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;

/**
 *
 * @author SPerkins
 */
public class ProteinAmbiguityGroupStandard implements ProteinAmbiguityGroup {
    private uk.ac.ebi.jmzidml.model.mzidml.ProteinAmbiguityGroup group;
    
    public ProteinAmbiguityGroupStandard(uk.ac.ebi.jmzidml.model.mzidml.ProteinAmbiguityGroup group) {
        this.group = group;
    }    

    @Override
    public List<ProteinDetectionHypothesis> getProteinDetectionHypothesis() {
        return group.getProteinDetectionHypothesis().stream().map(p -> new ProteinDetectionHypothesisStandard(p)).collect(Collectors.toList());
    }

    @Override
    public String getId() {
        return group.getId();
    }

    @Override
    public String getName() {
        return group.getName();
    }    
}

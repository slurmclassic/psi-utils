
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

/**
 *
 * @author SPerkins
 */
public interface SpectrumIdentificationList extends Iterable<SpectrumIdentificationResult> {
    String getId();
}

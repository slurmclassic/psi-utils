
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

import java.util.List;

/**
 *
 * @author SPerkins
 */
public interface MzIdentMLSingleStorage  {
    String getVersion();
    SequenceCollection getSequenceCollection();
    List<SpectrumIdentificationList> getSpectrumIdentificationLists();
    List<ProteinDetectionList> getProteinDetectionLists();
    AnalysisProtocolCollection getAnalysisProcolCollection();
    PeptideEvidence findPeptideEvidenceByRef(PeptideEvidenceRef ref);
    PeptideEvidence findPeptideEvidenceByRef(String ref);
    SpectrumIdentificationResult findSpectrumIdentificationResultByRef(String ref);
    SpectrumIdentificationItem findSpectrumIdentificationItemByRef(String ref);
    ProteinDetectionHypothesis findProteinDetectionHypothesisByRef(String ref);
    ProteinAmbiguityGroup findProteinAmbiguityGroupByRef(String ref);
    DBSequence findDBSequenceByRef(String ref);
    Peptide findPeptideByRef(String ref);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionProtocol;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationProtocol;

/**
 *
 * @author SPerkins
 */
public class AnalysisProtocolCollectionRelational implements AnalysisProtocolCollection {
    private int db_id;
    
    public AnalysisProtocolCollectionRelational(int db_id) {
        this.db_id = db_id;
    }

    @Override
    public List<SpectrumIdentificationProtocol> getSpectrumIdentificationProtocol() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProteinDetectionProtocol getProteinDetectionProtocol() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}

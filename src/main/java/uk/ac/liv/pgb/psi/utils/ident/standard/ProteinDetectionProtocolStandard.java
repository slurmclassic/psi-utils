/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.ParamList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionProtocol;

/**
 *
 * @author SPerkins
 */
public class ProteinDetectionProtocolStandard implements ProteinDetectionProtocol {
    private uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionProtocol protocol;
    
    public ProteinDetectionProtocolStandard(uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionProtocol protocol) {
        this.protocol = protocol;
    }

    @Override
    public ParamList getThreshold() {
        return new ParamListStandard(protocol.getThreshold());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SearchModification;

/**
 *
 * @author SPerkins
 */
public class SearchModificationStandard implements SearchModification {
    private uk.ac.ebi.jmzidml.model.mzidml.SearchModification modification;
    
    public SearchModificationStandard(uk.ac.ebi.jmzidml.model.mzidml.SearchModification modification) {
        this.modification = modification;
    }

    @Override
    public boolean isFixedMod() {
        return modification.isFixedMod();
    }

    @Override
    public float getMassDelta() {
        return modification.getMassDelta();
    }

    @Override
    public List<String> getResidues() {
        return modification.getResidues();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Modification;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;

/**
 *
 * @author SPerkins
 */
public class PeptideStandard implements Peptide {
    private uk.ac.ebi.jmzidml.model.mzidml.Peptide peptide;
    
    public PeptideStandard(uk.ac.ebi.jmzidml.model.mzidml.Peptide peptide) {
        this.peptide = peptide;
    }

    @Override
    public Iterator<Modification> iterator() {
        return new Iterator<Modification>() {
            Iterator<uk.ac.ebi.jmzidml.model.mzidml.Modification> localIterator = peptide.getModification().iterator();
            
            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public Modification next() {
                return new ModificationStandard(localIterator.next());
            }
        };
    }

    @Override
    public String getPeptideSequence() {
        return peptide.getPeptideSequence();
    }

    @Override
    public List<Modification> getModification() {
        return peptide.getModification().stream().map(p -> new ModificationStandard(p)).collect(Collectors.toList());
    }

    @Override
    public String getId() {
        return peptide.getId();
    }
    
}

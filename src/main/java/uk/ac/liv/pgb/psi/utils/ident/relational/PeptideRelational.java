/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.util.Iterator;
import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Modification;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;

/**
 *
 * @author SPerkins
 */
public class PeptideRelational implements Peptide {
    private final int db_id;
    private final String id;
    
    public PeptideRelational(int db_id, String id) {
        this.db_id = db_id;
        this.id = id;
    }

    @Override
    public String getPeptideSequence() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Modification> getModification() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Iterator<Modification> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.util.Iterator;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationListRelational implements SpectrumIdentificationList {
    private int db_id;
    private String id;
    
    public SpectrumIdentificationListRelational(int db_id, String id) {
        this.db_id = db_id;
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Iterator<SpectrumIdentificationResult> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;

/**
 *
 * @author SPerkins
 */
public class PeptideEvidenceRelational implements PeptideEvidence {
    private final int db_id;
    private final String id;
    private final boolean isDecoy;
    private final String post;
    private final String pre;
    private final int end;
    private final int start;
    private final String peptide_ref;
    private final String dbSequence_ref;
    
    public PeptideEvidenceRelational(int db_id,String id,boolean isDecoy,String post,String pre,int end,int start,String peptide_ref,String dbSequence_ref) {
        this.db_id = db_id;
        this.id = id;
        this.isDecoy = isDecoy;
        this.post = post;
        this.pre = pre;
        this.end = end;
        this.start = start;
        this.peptide_ref = peptide_ref;
        this.dbSequence_ref = dbSequence_ref;
    }

    @Override
    public int getStart() {
        return this.start;
    }

    @Override
    public int getEnd() {
        return this.end;
    }

    @Override
    public String getPre() {
        return this.pre;
    }

    @Override
    public String getPost() {
        return this.post;
    }

    @Override
    public boolean isDecoy() {
        return this.isDecoy;
    }

    @Override
    public String getDBSequenceRef() {
        return this.dbSequence_ref;
    }

    @Override
    public String getPeptideRef() {
        return this.peptide_ref;
    }
    
}

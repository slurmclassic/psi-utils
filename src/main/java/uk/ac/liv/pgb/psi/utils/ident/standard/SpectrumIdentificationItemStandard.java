
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Fragmentation;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationItemStandard implements SpectrumIdentificationItem {
    private final uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem item;
    
    public SpectrumIdentificationItemStandard(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem item) {
        this.item = item;
    }

    @Override
    public List<CvParam> getCvParam() {        
        return item.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());        
    }

    @Override
    public List<PeptideEvidenceRef> getPeptideEvidenceRef() {
        return item.getPeptideEvidenceRef().stream().map(p -> new PeptideEvidenceRefStandard(p)).collect(Collectors.toList());
    }

    @Override
    public double getCalculatedMassToCharge() {
        return item.getCalculatedMassToCharge();
    }

    @Override
    public String getId() {
        return item.getId();
    }

    @Override
    public double getExperimentalMassToCharge() {
        return item.getExperimentalMassToCharge();
    }

    @Override
    public int getRank() {
        return item.getRank();
    }

    @Override
    public boolean isPassThreshold() {
        return item.isPassThreshold();
    }

    @Override
    public String getPeptideRef() {
        return item.getPeptideRef();
    }

    @Override
    public int getChargeState() {
        return item.getChargeState();
    }

    @Override
    public String getName() {
        return item.getName();        
    }

    @Override
    public Fragmentation getFragmentation() {
        return new FragmentationStandard(item.getFragmentation());
    }
}

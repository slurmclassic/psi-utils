/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItemRef;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationItemRefStandard implements SpectrumIdentificationItemRef {
    private uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItemRef ref;
    
    public SpectrumIdentificationItemRefStandard(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItemRef ref) {
        this.ref = ref;
    }

    @Override
    public String getSpectrumIdentificationItemRef() {
        return ref.getSpectrumIdentificationItemRef();
    }
    
    
}

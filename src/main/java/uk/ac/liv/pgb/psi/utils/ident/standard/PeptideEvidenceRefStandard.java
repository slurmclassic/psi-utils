
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;

/**
 *
 * @author SPerkins
 */
public class PeptideEvidenceRefStandard implements PeptideEvidenceRef {
    private final uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidenceRef ref;
    
    public PeptideEvidenceRefStandard(uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidenceRef ref) {
        this.ref = ref;
    }

    @Override
    public String getRef() {
        return ref.getPeptideEvidenceRef();
    }    
}

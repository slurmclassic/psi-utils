/*
 * ProteoSuite is released under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard.utils;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author SPerkins
 */
public class ListFromIterableCachableImmutable<T> extends AbstractList<T> implements List<T> {

    private Iterable<T> iterable;
    private boolean sizeRead = false;
    private int size;

    public ListFromIterableCachableImmutable(Iterable<T> iterable, boolean cache) {
        this.iterable = iterable;
    }

    @Override
    public T get(int index) {
        int localIndex = 0;
        for (T t : iterable) {
            if (localIndex == index) {
                return t;
            }
            
            index++;
        }
        
        size = localIndex;
        sizeRead = true;
        
        throw new IndexOutOfBoundsException(String.valueOf(index));
    }

    @Override
    public int size() {
        if (!sizeRead) {
            int localSize = 0;
            for (T t : iterable) {
                localSize++;
            }
            
            size = localSize;
            sizeRead = true;            
        }
        
        return this.size;
    }

    @Override
    public Iterator<T> iterator() {
        return iterable.iterator();
    }
}

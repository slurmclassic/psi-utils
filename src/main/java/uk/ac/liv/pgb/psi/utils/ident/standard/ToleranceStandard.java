/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Tolerance;

/**
 *
 * @author SPerkins
 */
public class ToleranceStandard implements Tolerance {
    private uk.ac.ebi.jmzidml.model.mzidml.Tolerance tolerance;
    
    public ToleranceStandard(uk.ac.ebi.jmzidml.model.mzidml.Tolerance tolerance) {
        this.tolerance = tolerance;
    }

    @Override
    public List<CvParam> getCvParam() {
        return tolerance.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Modification;

/**
 *
 * @author SPerkins
 */
public class ModificationStandard implements Modification {
    private uk.ac.ebi.jmzidml.model.mzidml.Modification mod;
    
    public ModificationStandard(uk.ac.ebi.jmzidml.model.mzidml.Modification mod) {
        this.mod = mod;
    }    

    @Override
    public List<CvParam> getCvParam() {
        return mod.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());
    }

    @Override
    public int getLocation() {
        return mod.getLocation();
    }

    @Override
    public List<String> getResidues() {
        return mod.getResidues();
    }
    
}

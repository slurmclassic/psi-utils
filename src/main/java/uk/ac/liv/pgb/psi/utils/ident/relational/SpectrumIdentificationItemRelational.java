/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Fragmentation;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationItemRelational implements SpectrumIdentificationItem {
    private final int db_id;
    private final String id;
    private final String name;
    private final boolean isPassThreshold;
    private final int rank;
    private final String peptide_ref;
    private final double calculatedMassToCharge;
    private final double experimentalMassToCharge;
    private final int chargeState;
    
    public SpectrumIdentificationItemRelational(int db_id,String id,String name,boolean isPassThreshold,int rank,String peptide_ref,double calculatedMassToCharge,double experimentalMassToCharge,int chargeState) {
        this.db_id = db_id;
        this.id = id;
        this.name = name;
        this.isPassThreshold = isPassThreshold;
        this.rank = rank;
        this.peptide_ref = peptide_ref;
        this.calculatedMassToCharge = calculatedMassToCharge;
        this.experimentalMassToCharge = experimentalMassToCharge;
        this.chargeState = chargeState;
    }
    

    @Override
    public List<CvParam> getCvParam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PeptideEvidenceRef> getPeptideEvidenceRef() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getCalculatedMassToCharge() {
        return this.calculatedMassToCharge;
    }

    @Override
    public double getExperimentalMassToCharge() {
        return this.experimentalMassToCharge;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public int getRank() {
        return this.rank;
    }

    @Override
    public boolean isPassThreshold() {
        return this.isPassThreshold;
    }

    @Override
    public String getPeptideRef() {
        return this.peptide_ref;
    }

    @Override
    public int getChargeState() {
        return this.chargeState;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Fragmentation getFragmentation() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

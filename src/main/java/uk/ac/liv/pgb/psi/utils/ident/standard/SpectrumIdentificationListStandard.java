
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationListStandard implements SpectrumIdentificationList {
    private final uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationList list;
    
    public SpectrumIdentificationListStandard(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationList list) {
        this.list = list;
    }
    

    @Override
    public Iterator<SpectrumIdentificationResult> iterator() {
        return new Iterator<SpectrumIdentificationResult>() {
            private final Iterator<uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult> localIterator = list.getSpectrumIdentificationResult().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public SpectrumIdentificationResult next() {
                return new SpectrumIdentificationResultStandard(localIterator.next());
            }
        };
    }

    @Override
    public String getId() {
        return list.getId();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ParamList;

/**
 *
 * @author SPerkins
 */
public class ParamListStandard implements ParamList {
    private uk.ac.ebi.jmzidml.model.mzidml.ParamList paramList;
    
    public ParamListStandard(uk.ac.ebi.jmzidml.model.mzidml.ParamList paramList) {
        this.paramList = paramList;
    }
    
    @Override
    public List<CvParam> getCvParam() {
        return paramList.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());
    }
    
}

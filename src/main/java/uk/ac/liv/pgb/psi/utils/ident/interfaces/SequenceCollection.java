/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

import java.util.Iterator;

/**
 *
 * @author SPerkins
 */
public interface SequenceCollection {
    Iterator<DBSequence> getDBSequence();
    Iterator<Peptide> getPeptide();
}

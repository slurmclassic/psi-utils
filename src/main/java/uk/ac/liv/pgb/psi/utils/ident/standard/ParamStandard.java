/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Param;

/**
 *
 * @author SPerkins
 */
public class ParamStandard implements Param {
    private uk.ac.ebi.jmzidml.model.mzidml.Param param;
    
    public ParamStandard(uk.ac.ebi.jmzidml.model.mzidml.Param param) {
        this.param = param;
    }

    @Override
    public CvParam getCvParam() {
        return new CvParamStandard(param.getCvParam());
    }
    
    
    
}

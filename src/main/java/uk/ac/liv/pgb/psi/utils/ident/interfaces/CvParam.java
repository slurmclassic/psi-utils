
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

/**
 *
 * @author SPerkins
 */
public interface CvParam {
    String getAccession();
    String getValue();
    String getName();
    String getUnitName();
    String getUnitAccession();
}

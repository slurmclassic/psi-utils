/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SequenceCollection;

/**
 *
 * @author SPerkins
 */
public class SequenceCollectionStandard implements SequenceCollection {
    private uk.ac.ebi.jmzidml.model.mzidml.SequenceCollection collection;
    
    public SequenceCollectionStandard(uk.ac.ebi.jmzidml.model.mzidml.SequenceCollection collection) {
        this.collection = collection;
    }

    @Override
    public Iterator<DBSequence> getDBSequence() {
        return new Iterator<DBSequence>() {
            private Iterator<uk.ac.ebi.jmzidml.model.mzidml.DBSequence> localIterator = collection.getDBSequence().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public DBSequence next() {
                return new DBSequenceStandard(localIterator.next());
            }
        };
    }

    @Override
    public Iterator<Peptide> getPeptide() {
        return new Iterator<Peptide>() {
            private Iterator<uk.ac.ebi.jmzidml.model.mzidml.Peptide> localIterator = collection.getPeptide().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public Peptide next() {
                return new PeptideStandard(localIterator.next());
            }
        };
    }
    
}

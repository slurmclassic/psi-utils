/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzyme;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ParamList;

/**
 *
 * @author SPerkins
 */
public class EnzymeStandard implements Enzyme {
    private uk.ac.ebi.jmzidml.model.mzidml.Enzyme enzyme;
    
    public EnzymeStandard(uk.ac.ebi.jmzidml.model.mzidml.Enzyme enzyme) {
        this.enzyme = enzyme;
    }

    @Override
    public ParamList getEnzymeName() {
        return new ParamListStandard(enzyme.getEnzymeName());
    }
    
}

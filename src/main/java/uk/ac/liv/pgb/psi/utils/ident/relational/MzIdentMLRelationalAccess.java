
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisData;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.MzIdentMLSingleStorage;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SequenceCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class MzIdentMLRelationalAccess implements MzIdentMLSingleStorage {
    private static final MzidRelationalManager identManager = MzidRelationalManager.getInstance();    
    private int mzid_db_id;
    public MzIdentMLRelationalAccess(File mzidFile) {        
        try {    
            mzid_db_id = identManager.saveMzid(mzidFile);
        }
        catch (FileNotFoundException ex) {
            mzid_db_id = -1;
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }

    @Override
    public String getVersion() {
        try {
            return identManager.getMzidVersion(mzid_db_id);
        }
        catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }    

    @Override
    public List<SpectrumIdentificationList> getSpectrumIdentificationLists() {
        try {
            AnalysisData analysisData = identManager.getAnalysisData(mzid_db_id);
            if (analysisData != null) {
                return analysisData.getSpectrumIdentificationLists();
            }           
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Collections.emptyList();
    }

    @Override
    public PeptideEvidence findPeptideEvidenceByRef(PeptideEvidenceRef ref) {
        try {
            PeptideEvidence evidence = identManager.findPeptideEvidenceByRef(mzid_db_id, ref.getRef());
            return evidence;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public SpectrumIdentificationResult findSpectrumIdentificationResultByRef(String ref) {
        try {
            SpectrumIdentificationResult result = identManager.findSpectrumIdentificationResultByRef(mzid_db_id, ref);
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public Peptide findPeptideByRef(String ref) {
        try {
            Peptide peptide = identManager.findPeptideByRef(mzid_db_id, ref);
            return peptide;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public List<ProteinDetectionList> getProteinDetectionLists() {
        try {
            AnalysisData analysisData = identManager.getAnalysisData(mzid_db_id);
            if (analysisData != null) {
                return analysisData.getProteinDetectionLists();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Collections.emptyList();
    }

    @Override
    public AnalysisProtocolCollection getAnalysisProcolCollection() {
        try {
            AnalysisProtocolCollection collection = identManager.getAnalysisProtocolCollection(mzid_db_id);
            return collection;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public PeptideEvidence findPeptideEvidenceByRef(String ref) {
        try {
            PeptideEvidence evidence = identManager.findPeptideEvidenceByRef(mzid_db_id, ref);
            return evidence;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public SpectrumIdentificationItem findSpectrumIdentificationItemByRef(String ref) {
        try {
            SpectrumIdentificationItem item = identManager.findSpectrumIdentificationItemByRef(mzid_db_id, ref);
            return item;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    @Override
    public ProteinDetectionHypothesis findProteinDetectionHypothesisByRef(String ref) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProteinAmbiguityGroup findProteinAmbiguityGroupByRef(String ref) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DBSequence findDBSequenceByRef(String ref) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SequenceCollection getSequenceCollection() {
        try {
            SequenceCollection collection = identManager.getSequenceCollection(mzid_db_id);
            return collection;
        } catch (SQLException ex) {
            Logger.getLogger(MzIdentMLRelationalAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }    
}

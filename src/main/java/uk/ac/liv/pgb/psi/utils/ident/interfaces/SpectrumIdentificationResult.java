
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

import java.util.List;

/**
 *
 * @author SPerkins
 */
public interface SpectrumIdentificationResult extends Iterable<SpectrumIdentificationItem>{
    List<CvParam> getCvParam();    
    String getSpectrumID();
    String getId();
}

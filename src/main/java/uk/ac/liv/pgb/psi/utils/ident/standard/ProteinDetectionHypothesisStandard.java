/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;

/**
 *
 * @author SPerkins
 */
public class ProteinDetectionHypothesisStandard implements ProteinDetectionHypothesis {
    private uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionHypothesis hypothesis;
    
    public ProteinDetectionHypothesisStandard(uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionHypothesis hypothesis) {
        this.hypothesis = hypothesis;
    }

    @Override
    public String getDBSequenceRef() {
        return hypothesis.getDBSequenceRef();
    }

    @Override
    public List<PeptideHypothesis> getPeptideHypothesis() {
        return hypothesis.getPeptideHypothesis().stream().map(p -> new PeptideHypothesisStandard(p)).collect(Collectors.toList());
    }

    @Override
    public List<CvParam> getCvParam() {
        return hypothesis.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());
    }

    @Override
    public boolean isPassThreshold() {
        return hypothesis.isPassThreshold();
    }

    @Override
    public String getId() {
        return hypothesis.getId();
    }
    
}

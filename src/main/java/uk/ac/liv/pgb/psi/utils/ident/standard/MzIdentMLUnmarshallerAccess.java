
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import uk.ac.ebi.jmzidml.MzIdentMLElement;
import uk.ac.ebi.jmzidml.xml.io.MzIdentMLUnmarshaller;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.MzIdentMLSingleStorage;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SequenceCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;
import uk.ac.liv.pgb.psi.utils.ident.standard.utils.CollectionUtils;

/**
 *
 * @author SPerkins
 */
public class MzIdentMLUnmarshallerAccess implements MzIdentMLSingleStorage {
    private final MzIdentMLUnmarshaller unmarshaller;
    private List<SpectrumIdentificationList> silCache = null;
    
    public MzIdentMLUnmarshallerAccess(File mzidFile) {
        this.unmarshaller = new MzIdentMLUnmarshaller(mzidFile);
    }

    @Override
    public String getVersion() {
        return unmarshaller.getMzIdentMLVersion();        
    }

    @Override
    public List<SpectrumIdentificationList> getSpectrumIdentificationLists() {
        if (silCache == null) {
            Iterator<uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationList> iterator = unmarshaller.unmarshalCollectionFromXpath(MzIdentMLElement.SpectrumIdentificationList);
            silCache = CollectionUtils.listFromIteratorFullCopy(iterator).stream().map(p -> new SpectrumIdentificationListStandard(p)).collect(Collectors.toList());
        }
        
        return silCache;
    }

    @Override
    public PeptideEvidence findPeptideEvidenceByRef(PeptideEvidenceRef ref) {
        return this.findPeptideEvidenceByRef(ref.getRef());
    }    

    @Override
    public SpectrumIdentificationResult findSpectrumIdentificationResultByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult result = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult.class,
                                ref);
            return new SpectrumIdentificationResultStandard(result);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find SpectrumIdentificationResult with id: " + ref);
                return null;
        }
    }

    @Override
    public Peptide findPeptideByRef(String ref) {        
        try {
            uk.ac.ebi.jmzidml.model.mzidml.Peptide peptide = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.Peptide.class,
                                ref);
            return new PeptideStandard(peptide);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find Peptide with id: " + ref);
                return null;
        }
    }

    @Override
    public AnalysisProtocolCollection getAnalysisProcolCollection() {
        uk.ac.ebi.jmzidml.model.mzidml.AnalysisProtocolCollection collection = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.AnalysisProtocolCollection.class);
        return new AnalysisProtocolCollectionStandard(collection);
    }

    @Override
    public ProteinDetectionHypothesis findProteinDetectionHypothesisByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionHypothesis hypothesis = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionHypothesis.class,
                                ref);
            return new ProteinDetectionHypothesisStandard(hypothesis);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find ProteinDetectionHypothesis with id: " + ref);
                return null;
        }
    }

    @Override
    public DBSequence findDBSequenceByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.DBSequence dbseq = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.DBSequence.class,
                                ref);
            return new DBSequenceStandard(dbseq);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find DBSequence with id: " + ref);
                return null;
        }
    }

    @Override
    public SpectrumIdentificationItem findSpectrumIdentificationItemByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem item = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem.class,
                                ref);
            return new SpectrumIdentificationItemStandard(item);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find SpectrumIdentificationItem with id: " + ref);
                return null;
        }
    }

    @Override
    public ProteinAmbiguityGroup findProteinAmbiguityGroupByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.ProteinAmbiguityGroup group = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.ProteinAmbiguityGroup.class,
                                ref);
            return new ProteinAmbiguityGroupStandard(group);
        } catch (JAXBException ex) {
             System.out.println("Warning: unable to find SpectrumIdentificationItem with id: " + ref);
                return null;
        }
    }

    @Override
    public PeptideEvidence findPeptideEvidenceByRef(String ref) {
        try {
            uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence evidence = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence.class,
                                ref);
            return new PeptideEvidenceStandard(evidence);
        } catch (JAXBException ex) {
            System.out.println("Warning: unable to find PeptideEvidence with id: " + ref);
            return null;
        }   
    }

    @Override
    public List<ProteinDetectionList> getProteinDetectionLists() {
        Iterator<uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionList> iterator = unmarshaller.unmarshalCollectionFromXpath(MzIdentMLElement.ProteinDetectionList);
        return CollectionUtils.listFromIteratorFullCopy(iterator).stream().map(p -> new ProteinDetectionListStandard(p)).collect(Collectors.toList());
    }

    @Override
    public SequenceCollection getSequenceCollection() {
        uk.ac.ebi.jmzidml.model.mzidml.SequenceCollection collection = unmarshaller.unmarshal(uk.ac.ebi.jmzidml.model.mzidml.SequenceCollection.class);
        return new SequenceCollectionStandard(collection);        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;

/**
 *
 * @author SPerkins
 */
public class DBSequenceStandard implements DBSequence {
    private uk.ac.ebi.jmzidml.model.mzidml.DBSequence dbseq;
    
    public DBSequenceStandard(uk.ac.ebi.jmzidml.model.mzidml.DBSequence dbseq) {
        this.dbseq = dbseq;
    }

    @Override
    public List<CvParam> getCvParam() {
        return dbseq.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());
    }

    @Override
    public String getSeq() {
        return dbseq.getSeq();
    }

    @Override
    public String getAccession() {
        return dbseq.getAccession();
    }

    @Override
    public String getId() {
        return dbseq.getId();
    }
}

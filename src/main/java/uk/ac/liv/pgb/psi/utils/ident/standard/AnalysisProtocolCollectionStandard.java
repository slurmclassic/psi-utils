/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionProtocol;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationProtocol;

/**
 *
 * @author SPerkins
 */
public class AnalysisProtocolCollectionStandard implements AnalysisProtocolCollection {
    private uk.ac.ebi.jmzidml.model.mzidml.AnalysisProtocolCollection collection;
    
    public AnalysisProtocolCollectionStandard(uk.ac.ebi.jmzidml.model.mzidml.AnalysisProtocolCollection collection) {
        this.collection = collection;
    }

    @Override
    public List<SpectrumIdentificationProtocol> getSpectrumIdentificationProtocol() {
        return collection.getSpectrumIdentificationProtocol().stream().map(p -> new SpectrumIdentificationProtocolStandard(p)).collect(Collectors.toList());
    }

    @Override
    public ProteinDetectionProtocol getProteinDetectionProtocol() {
        return new ProteinDetectionProtocolStandard(collection.getProteinDetectionProtocol());
    }
    
}

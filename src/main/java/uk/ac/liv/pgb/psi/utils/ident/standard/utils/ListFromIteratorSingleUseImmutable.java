/*
 * ProteoSuite is released under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard.utils;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author SPerkins
 */
public class ListFromIteratorSingleUseImmutable<T> extends AbstractList<T> implements List<T> {
    private Iterator<T> iterator;
    private boolean used = false;
    
    public ListFromIteratorSingleUseImmutable(Iterator<T> iterator) {
        this.iterator = iterator;
    }
    
    @Override
    public T get(int index) {
        if (used) {
            throw new UnsupportedOperationException("Method cannot be used. This list can only be used once, and has been used.");
        }
        
        int localIndex = 0;
        
        while (iterator.hasNext()) {
            T t = iterator.next();
            if (localIndex == index) {
                return t;
            }
            
            localIndex++;
        }
        
        throw new IndexOutOfBoundsException(String.valueOf(index));
    }

    @Override
    public int size() {
        if (used) {
            throw new UnsupportedOperationException("Method cannot be used. This list can only be used once, and has been used.");
        }
        
        int size = 0;
        while (iterator.hasNext()) {
            iterator.next();
            size++;
        }
        
        used = true;
        return size;
    }
    
    @Override
    public Iterator<T> iterator() {
        if (used) {
            throw new UnsupportedOperationException("Method cannot be used. This list can only be used once, and has been used.");
        }
        
        return this.iterator;    
    }
}

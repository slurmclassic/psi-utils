
package uk.ac.liv.pgb.psi.utils.ident.interfaces;

/**
 *
 * @author SPerkins
 */
public interface PeptideEvidence {
    int getStart();
    int getEnd();
    String getPre();
    String getPost();
    boolean isDecoy();
    String getDBSequenceRef();
    String getPeptideRef();
}

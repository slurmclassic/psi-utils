
package uk.ac.liv.pgb.psi.utils.ident;


import java.io.File;
import uk.ac.liv.pgb.psi.utils.ident.standard.MzIdentMLUnmarshallerAccess;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.MzIdentMLSingleStorage;

/**
 *
 * @author SPerkins
 */
public class MzIdentMLSingleStorageFactory {
    private static final MzIdentMLSingleStorageFactory INSTANCE = new MzIdentMLSingleStorageFactory();
    
    public static MzIdentMLSingleStorageFactory getInstance() {
        return INSTANCE;
    }
    
    public MzIdentMLSingleStorage getSingleStorage(File mzidFile) {
        return new MzIdentMLUnmarshallerAccess(mzidFile);
    }
}

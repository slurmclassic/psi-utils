/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzymes;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ModificationParams;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Param;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ParamList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationProtocol;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Tolerance;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationProtocolStandard implements SpectrumIdentificationProtocol {
    private uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationProtocol protocol;
    
    public SpectrumIdentificationProtocolStandard(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationProtocol protocol) {
        this.protocol = protocol;        
    }

    @Override
    public Param getSearchType() {
        return new ParamStandard(protocol.getSearchType());
    }

    @Override
    public Enzymes getEnzymes() {
        return new EnzymesStandard(protocol.getEnzymes());
    }    

    @Override
    public ModificationParams getModificationParams() {
        return new ModificationParamsStandard(protocol.getModificationParams());
    }

    @Override
    public Tolerance getFragmentTolerance() {
        return new ToleranceStandard(protocol.getFragmentTolerance());
    }

    @Override
    public Tolerance getParentTolerance() {
        return new ToleranceStandard(protocol.getParentTolerance());
    }

    @Override
    public ParamList getThreshold() {
        return new ParamListStandard(protocol.getThreshold());
    }
}

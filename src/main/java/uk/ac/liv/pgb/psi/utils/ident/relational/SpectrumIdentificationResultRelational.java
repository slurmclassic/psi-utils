/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.util.Iterator;
import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationResultRelational implements SpectrumIdentificationResult {
    private final int db_id;
    private final String id;
    private final String spectrumID;
    
    public SpectrumIdentificationResultRelational(int db_id, String id, String spectrumID) {
        this.db_id = db_id;
        this.id = id;
        this.spectrumID = spectrumID;
    }

    @Override
    public List<CvParam> getCvParam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getSpectrumID() {
        return this.spectrumID;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Iterator<SpectrumIdentificationItem> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.FragmentArray;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.IonType;

/**
 *
 * @author SPerkins
 */
public class IonTypeStandard implements IonType {
    private uk.ac.ebi.jmzidml.model.mzidml.IonType ionType;
    
    public IonTypeStandard(uk.ac.ebi.jmzidml.model.mzidml.IonType ionType) {
        this.ionType = ionType;
    }

    @Override
    public CvParam getCvParam() {
        return new CvParamStandard(ionType.getCvParam());
    }

    @Override
    public int getCharge() {
        return ionType.getCharge();
    }

    @Override
    public List<Integer> getIndex() {
        return ionType.getIndex();
    }

    @Override
    public List<FragmentArray> getFragmentArray() {
        return ionType.getFragmentArray().stream().map(p -> new FragmentArrayStandard(p)).collect(Collectors.toList());
    }
}

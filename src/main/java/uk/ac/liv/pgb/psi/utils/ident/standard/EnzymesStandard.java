/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzyme;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzymes;

/**
 *
 * @author SPerkins
 */
public class EnzymesStandard implements Enzymes {
    private uk.ac.ebi.jmzidml.model.mzidml.Enzymes enzymes;
    
    public EnzymesStandard(uk.ac.ebi.jmzidml.model.mzidml.Enzymes enzymes) {
        this.enzymes = enzymes;
    }

    @Override
    public Iterator<Enzyme> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Enzyme> getEnzyme() {
        return enzymes.getEnzyme().stream().map(p -> new EnzymeStandard(p)).collect(Collectors.toList());
    }
    
}

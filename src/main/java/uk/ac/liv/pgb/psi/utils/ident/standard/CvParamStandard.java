
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;

/**
 *
 * @author SPerkins
 */
public class CvParamStandard implements CvParam {
    private final uk.ac.ebi.jmzidml.model.mzidml.CvParam param;
    public CvParamStandard(uk.ac.ebi.jmzidml.model.mzidml.CvParam param) {
        this.param = param;
    }

    @Override
    public String getAccession() {
        return param.getAccession();
    }

    @Override
    public String getValue() {
        return param.getValue();
    }

    @Override
    public String getUnitName() {
        return param.getUnitName();
    }

    @Override
    public String getUnitAccession() {
        return param.getAccession();
    }    

    @Override
    public String getName() {
        return param.getName();
    }
}

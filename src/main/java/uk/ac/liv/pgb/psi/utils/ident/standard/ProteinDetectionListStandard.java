/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionList;

/**
 *
 * @author SPerkins
 */
public class ProteinDetectionListStandard implements ProteinDetectionList {
    private uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionList list;
    
    public ProteinDetectionListStandard(uk.ac.ebi.jmzidml.model.mzidml.ProteinDetectionList list) {
        this.list = list;
    }

    @Override
    public Iterator<ProteinAmbiguityGroup> iterator() {
        return new Iterator<ProteinAmbiguityGroup>() {
            private final Iterator<uk.ac.ebi.jmzidml.model.mzidml.ProteinAmbiguityGroup> localIterator = list.getProteinAmbiguityGroup().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public ProteinAmbiguityGroup next() {
                return new ProteinAmbiguityGroupStandard(localIterator.next());
            }
        };
    }
    
}

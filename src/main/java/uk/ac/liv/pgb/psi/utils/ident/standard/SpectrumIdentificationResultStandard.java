
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class SpectrumIdentificationResultStandard implements SpectrumIdentificationResult {
    private final uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult result;
    
    public SpectrumIdentificationResultStandard(uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult result) {
        this.result = result;
    }

    @Override
    public Iterator<SpectrumIdentificationItem> iterator() {
        return new Iterator<SpectrumIdentificationItem>() {
            private Iterator<uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem> localIterator = result.getSpectrumIdentificationItem().iterator();

            @Override
            public boolean hasNext() {
                return localIterator.hasNext();
            }

            @Override
            public SpectrumIdentificationItem next() {
                return new SpectrumIdentificationItemStandard(localIterator.next());
            }            
        };
    }

    @Override
    public List<CvParam> getCvParam() {
        return result.getCvParam().stream().map(p -> new CvParamStandard(p)).collect(Collectors.toList());        
    }

    @Override
    public String getSpectrumID() {
        return result.getSpectrumID();
    }

    @Override
    public String getId() {
        return result.getId();
    }
}

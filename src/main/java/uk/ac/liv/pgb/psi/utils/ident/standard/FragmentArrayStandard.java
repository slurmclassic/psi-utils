/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.FragmentArray;

/**
 *
 * @author SPerkins
 */
public class FragmentArrayStandard implements FragmentArray {
    private uk.ac.ebi.jmzidml.model.mzidml.FragmentArray fragArray;
    
    public FragmentArrayStandard(uk.ac.ebi.jmzidml.model.mzidml.FragmentArray fragArray) {
        this.fragArray = fragArray;        
    }

    @Override
    public List<Float> getValues() {
        return fragArray.getValues();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author SPerkins
 */
public class CollectionUtils {
    public static <T> Iterable<T> concat(List<? extends Iterable<T>> iterables) {
        return new Iterable<T>() {
            @Override
            public Iterator<T> iterator() {
                return new Iterator<T>() {
                    private Iterator<? extends Iterable<T>> iterablesIterator = iterables.iterator();
                    private Iterator<T> localIterator = null;

                    @Override
                    public boolean hasNext() {
                        if (localIterator == null) {
                            if (iterablesIterator.hasNext()) {
                                localIterator = iterablesIterator.next().iterator();
                            } else {
                                return false;
                            }
                        }

                        while (!localIterator.hasNext()) {
                            if (iterablesIterator.hasNext()) {
                                localIterator = iterablesIterator.next().iterator();
                            } else {
                                return false;
                            }
                        }

                        return true;
                    }

                    @Override
                    public T next() {
                        return localIterator.next();
                    }
                };
            }
        };
    }

    public static <T> int sizeOfIterable(Iterable<T> iterable) {
        int size = 0;
        for (T element : iterable) {
            size++;
        }
        
        return size;
    }
    
    public static final <T> List<T> listFromIterableFullCopy(Iterable<T> iterable) {
        List<T> list = new LinkedList<>();
        Iterator<T> iterator = iterable.iterator();
        while(iterator.hasNext()) {
            list.add(iterator.next());
        }
        
        return list;
    }
    
    public static final <T> List<T> listFromIteratorFullCopy(Iterator<T> iterator) {
        List<T> list = new LinkedList<>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        
        return list;
    }
    
    public static final <T> List<T> listFromIterableNoCopyImmutable(Iterable<T> iterable) {
        return new ListFromIterableCachableImmutable(iterable, false);
    }  
    
    public static final <T> List<T> listFromIteratorSingleUse(Iterator<T> iterator) {
        return new ListFromIteratorSingleUseImmutable(iterator);
    }  
}


package uk.ac.liv.pgb.psi.utils.ident.interfaces;

import java.util.List;

/**
 *
 * @author SPerkins
 */
public interface SpectrumIdentificationItem {
    List<CvParam> getCvParam();
    List<PeptideEvidenceRef> getPeptideEvidenceRef();
    double getCalculatedMassToCharge();
    double getExperimentalMassToCharge();
    String getId();
    int getRank();
    boolean isPassThreshold();
    String getPeptideRef();
    int getChargeState();
    String getName();
    Fragmentation getFragmentation();
}

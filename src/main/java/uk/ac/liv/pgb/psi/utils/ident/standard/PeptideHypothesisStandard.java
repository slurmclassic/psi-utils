/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItemRef;

/**
 *
 * @author SPerkins
 */
public class PeptideHypothesisStandard implements PeptideHypothesis {
    private uk.ac.ebi.jmzidml.model.mzidml.PeptideHypothesis hypothesis;
    
    public PeptideHypothesisStandard(uk.ac.ebi.jmzidml.model.mzidml.PeptideHypothesis hypothesis) {
        this.hypothesis = hypothesis;        
    }

    @Override
    public List<SpectrumIdentificationItemRef> getSpectrumIdentificationItemRef() {
        return hypothesis.getSpectrumIdentificationItemRef().stream().map(p -> new SpectrumIdentificationItemRefStandard(p)).collect(Collectors.toList());
    }

    @Override
    public String getPeptideEvidenceRef() {
        return hypothesis.getPeptideEvidenceRef();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.ident.standard;

import java.util.List;
import java.util.stream.Collectors;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ModificationParams;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SearchModification;

/**
 *
 * @author SPerkins
 */
public class ModificationParamsStandard implements ModificationParams {
    private uk.ac.ebi.jmzidml.model.mzidml.ModificationParams params;
    
    public ModificationParamsStandard(uk.ac.ebi.jmzidml.model.mzidml.ModificationParams params) {
        this.params = params;
    }

    @Override
    public List<SearchModification> getSearchModification() {
        return params.getSearchModification().stream().map(p -> new SearchModificationStandard(p)).collect(Collectors.toList());
    }    
}

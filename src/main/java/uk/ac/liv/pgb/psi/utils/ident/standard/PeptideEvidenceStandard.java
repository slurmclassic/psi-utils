
package uk.ac.liv.pgb.psi.utils.ident.standard;

import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;

/**
 *
 * @author SPerkins
 */
public class PeptideEvidenceStandard implements PeptideEvidence {
    private final uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence evidence;
    
    public PeptideEvidenceStandard(uk.ac.ebi.jmzidml.model.mzidml.PeptideEvidence evidence) {
        this.evidence = evidence;
    }

    @Override
    public boolean isDecoy() {
        return evidence.isIsDecoy();
    }    

    @Override
    public int getStart() {
        return evidence.getStart();
    }

    @Override
    public int getEnd() {
        return evidence.getEnd();        
    }

    @Override
    public String getPre() {
        return evidence.getPre();
    }

    @Override
    public String getPost() {
        return evidence.getPost();
    }

    @Override
    public String getDBSequenceRef() {
        return evidence.getDBSequenceRef();
    }

    @Override
    public String getPeptideRef() {
        return evidence.getPeptideRef();
    }
}

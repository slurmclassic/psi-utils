
package uk.ac.liv.pgb.psi.utils.ident.relational;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import uk.ac.ebi.jmzidml.model.mzidml.AnalysisCollection;

import uk.ac.liv.pgb.psi.utils.config.Config;
import uk.ac.liv.pgb.psi.utils.config.Config.DatabaseEngine;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisData;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SequenceCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author SPerkins
 */
public class MzidRelationalManager {
    private static final MzidRelationalManager INSTANCE = new MzidRelationalManager();
    private static final Config config = Config.getInstance();
    private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    private Connection mzid_conn;
    private Map<String, PreparedStatement> dmlStatements = new HashMap<>();   
    
    private MzidRelationalManager() {
        try {
            String connectionString = null;
            String user = null;
            String password = null;
            String ddlRelativeLocation = null;
            String dmlRelativeLocation = null;
            if (config.getDatabaseEngine() == DatabaseEngine.HSQLDB) {
                String userHome = System.getProperty("user.home");
                Files.createDirectory(Paths.get(userHome + ".proteosuite"));                
                
                connectionString = "jdbc:hsqldb:file:" + userHome + ".proteosuite/mzid_db";
                user = "sa";
                password = "";
                ddlRelativeLocation = "hsqldb/DDL.txt";
                dmlRelativeLocation = "hsqldb/DML.txt";
                
                // Clean up previous sessions data files.                
                Files.deleteIfExists(Paths.get(userHome + ".proteosuite/mzid_db.log"));
                Files.deleteIfExists(Paths.get(userHome + ".proteosuite/mzid_db.properties"));
                Files.deleteIfExists(Paths.get(userHome + ".proteosuite/mzid_db.lck"));
                Files.deleteIfExists(Paths.get(userHome + ".proteosuite/mzid_db.script"));
                Files.deleteIfExists(Paths.get(userHome + ".proteosuite/mzid_db.data"));
            } else if (config.getDatabaseEngine() == DatabaseEngine.MYSQL) {
                connectionString = "jdbc:mysql:" + config.getDatabaseHost();
                user = config.getDatabaseUser();
                password = config.getDatabasePass();
                ddlRelativeLocation = "mysql/DDL.txt";
                dmlRelativeLocation = "mysql/DML.txt";
            } else {
                throw new UnsupportedOperationException(config.getDatabaseEngine().name() + " is not currently supported.");
            }
            
            // Get our connection.
            mzid_conn = DriverManager.getConnection(connectionString, user, password);       
            
            // Find the resource file which contains our DDL - table definitions.
            String ddlResourceString = getClass().getClassLoader().getResource(ddlRelativeLocation).getPath().substring(1); 
            
            // Find the resource file whuch contains our DML - manipulation statements.
            String dmlResourceString = getClass().getClassLoader().getResource(dmlRelativeLocation).getPath().substring(1); 
            
            // Get our DDL statements.
            List<String> ddl = Files.readAllLines(Paths.get(ddlResourceString)).stream().filter(p -> !p.isEmpty()).collect(Collectors.toList());
            for (String ddlStatement : ddl) {
                System.out.println("Executing: " + ddlStatement);
                // Execute the DDL statement.
                PreparedStatement createTableStatement = mzid_conn.prepareStatement(ddlStatement);
                createTableStatement.execute();
                createTableStatement.close();
            }           
            
            // Get our DML statements.
            List<String> dml = Files.readAllLines(Paths.get(dmlResourceString)).stream().filter(p -> !p.isEmpty()).collect(Collectors.toList());
            for (String dmlStatement : dml) {
                String[] parts = dmlStatement.split("\\t");
                if (parts.length != 3) {
                    continue;
                }
                
                System.out.println("Preparing: " + parts[0]);
                if (Boolean.parseBoolean(parts[2])) {
                    PreparedStatement statement = mzid_conn.prepareStatement(parts[1], Statement.RETURN_GENERATED_KEYS);
                    dmlStatements.put(parts[0], statement);
                } else {
                    PreparedStatement statement = mzid_conn.prepareStatement(parts[1]);
                    dmlStatements.put(parts[0], statement);
                }
            }           
                 
                       
        }
        catch (SQLException | IOException ex) {
            Logger.getLogger(MzidRelationalManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static MzidRelationalManager getInstance() {
        return INSTANCE;
    }
    
    public synchronized int saveMzid(File source) throws FileNotFoundException {
        
        try {
            XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(new FileInputStream(source));
            
            while (reader.hasNext()) {
                reader.next();
                if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                    if (reader.getLocalName().equalsIgnoreCase("mzidentml")) {
                        int mzid_id = createMzid(reader);               
                        return mzid_id;
                    }
                }
                
            }
        }
        catch (XMLStreamException ex) { 
            Logger.getLogger(MzidRelationalManager.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Error reading mzid file. XMLStreamException: " + ex.getLocalizedMessage());
        }
        catch (SQLException ex) {
            Logger.getLogger(MzidRelationalManager.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Error saving mzid file. SQLException: " + ex.getLocalizedMessage());
        }
        return -1;
    }   
    
    private int createMzid(XMLStreamReader reader) throws SQLException, XMLStreamException {   
        PreparedStatement statement = dmlStatements.get("createMzidStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "name"));
        statement.setString(3, reader.getAttributeValue(null, "version"));              
        int mzid_id = executeUpdateAndGetGeneratedId(statement);        
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvlist")) {
                    createCvList(reader, mzid_id);
                } else if (reader.getLocalName().equalsIgnoreCase("analysissoftwarelist")) {
                    createAnalysisSoftwareList(reader, mzid_id);
                } else if (reader.getLocalName().equalsIgnoreCase("provider")) {
                    createProvider(reader, mzid_id);
                } else if (reader.getLocalName().equalsIgnoreCase("auditcollection")) {
                    createAuditCollection(reader, mzid_id);
                } else if (reader.getLocalName().equalsIgnoreCase("sequencecollection")) {
                    createSequenceCollection(reader, mzid_id);
                } else if (reader.getLocalName().equalsIgnoreCase("datacollection")) {
                    createDataCollection(reader, mzid_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("mzidentml")) {
                    return mzid_id;
                }
            }
        }   
        
        return mzid_id;
    }
    
    private void createCvList(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createCvListStatement");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        int cv_list_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cv")) {
                    createCv(reader, cv_list_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvlist")) {
                    return;
                }
            }
        }
    }  

    private void createAnalysisSoftwareList(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createAnalysisSoftwareListStatement");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        int as_list_id = executeUpdateAndGetGeneratedId(statement);

        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("analysissoftware")) {
                    createAnalysisSoftware(reader, as_list_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("analysissoftwarelist")) {
                    return;
                }
            }
        }        
    }

    private void createProvider(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createProviderStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setInt(2, mzid_id);
        int provider_id = executeUpdateAndGetGeneratedId(statement);

        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("contactrole")) {
                    createContactRole(reader, provider_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("provider")) {
                    return;
                }
            }
        }        
    }

    private void createAuditCollection(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createAuditCollectionStatement");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        int audit_collection_id = executeUpdateAndGetGeneratedId(statement);
        
        while(reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("person")) {
                    createPerson(reader, audit_collection_id);
                } else if (reader.getLocalName().equalsIgnoreCase("organization")) {
                    createOrganisation(reader, audit_collection_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("auditcollection")) {
                    return;
                }
            }
        }        
    }

    private void createSequenceCollection(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createSequenceCollectionStatement");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        int sequence_collection_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("dbsequence")) {
                    createDbSequence(reader, sequence_collection_id);
                } else if (reader.getLocalName().equalsIgnoreCase("peptide")) {
                    createPeptide(reader, sequence_collection_id);
                } else if (reader.getLocalName().equalsIgnoreCase("peptideevidence")) {
                    createPeptideEvidence(reader, sequence_collection_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("sequencecollection")) {
                    return;
                }
            }
        }       
    }

    private void createAnalysisCollection(AnalysisCollection collection, int mzid_id) {

    }

    private void createAnalysisProtocolCollection(AnalysisProtocolCollection collection, int mzid_id) {

    }

    private void createDataCollection(XMLStreamReader reader, int mzid_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createDataCollectionStatement");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        int data_collection_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("analysisdata")) {
                    createAnalysisData(reader, data_collection_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("datacollection")) {
                    return;
                }
            }
        }        
    }

    private void createAnalysisData(XMLStreamReader reader, int data_collection_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createAnalysisDataStatement");
        statement.clearParameters();
        statement.setInt(1, data_collection_id);
        int analysis_data_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationlist")) {
                    createSpectrumIdentificationList(reader, analysis_data_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("analysisdata")) {
                    return;
                }
            }
        }        
    }

    private void createSpectrumIdentificationList(XMLStreamReader reader, int analysis_data_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createSpectrumIdentificationListStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setInt(2, analysis_data_id);
        int spectrum_identification_list_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationresult")) {
                    createSpectrumIdentificationResult(reader, spectrum_identification_list_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationlist")) {
                    return;
                }
            }
        }        
    }

    private void createSpectrumIdentificationResult(XMLStreamReader reader, int spectrum_identification_list_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createSpectrumIdentificationResultStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "spectrumID"));
        statement.setString(3, reader.getAttributeValue(null, "spectraData_ref"));
        statement.setInt(4, spectrum_identification_list_id);
        int spectrum_identification_result_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationitem")) {
                    createSpectrumIdentificationItem(reader, spectrum_identification_result_id);
                } else if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    CvParam param = createCvParam(reader, spectrum_identification_result_id, CvParamParent.SPECTRUM_IDENTIFICATION_RESULT);
                    if (param.getAccession().equalsIgnoreCase("MS:1000016")) {
                        double rtInSeconds = param.getUnitName().equalsIgnoreCase("second")? Double.parseDouble(param.getValue()) : Double.parseDouble(param.getValue())*60.0;
                        PreparedStatement updateStatement = dmlStatements.get("updateSpectrumIdentificationResultStatement");
                        updateStatement.clearParameters();
                        updateStatement.setDouble(1, rtInSeconds);
                        updateStatement.setInt(2, spectrum_identification_result_id);
                        updateStatement.executeUpdate();
                    }
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationresult")) {
                    return;
                }
            }
        }       
    }

    private void createSpectrumIdentificationItem(XMLStreamReader reader, int spectrum_identification_result_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createSpectrumIdentificationItemStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setBoolean(2, Boolean.parseBoolean(reader.getAttributeValue(null, "passThreshold")));
        statement.setInt(3, Integer.parseInt(reader.getAttributeValue(null, "rank")));
        statement.setString(4, reader.getAttributeValue(null, "peptide_ref"));
        statement.setDouble(5, Double.parseDouble(reader.getAttributeValue(null, "calculatedMassToCharge")));
        statement.setDouble(6, Double.parseDouble(reader.getAttributeValue(null, "experimentalMassToCharge")));
        statement.setInt(7, Integer.parseInt(reader.getAttributeValue(null, "chargeState")));
        statement.setInt(8, spectrum_identification_result_id);
        int spectrum_identification_item_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("peptideevidenceref")) {
                    createPeptideEvidenceRef(reader, spectrum_identification_item_id);
                } else if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, spectrum_identification_item_id, CvParamParent.SPECTRUM_IDENTIFICATION_ITEM);
                } else if (reader.getLocalName().equalsIgnoreCase("userparam")) {
                    createUserParam(reader, spectrum_identification_item_id, UserParamParent.SPECTRUM_IDENTIFICATION_ITEM);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("spectrumidentificationitem")) {
                    return;
                }
            }
        }        
    }

    private void createPeptideEvidenceRef(XMLStreamReader reader, int spectrum_identification_item_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("createPeptideEvidenceRefStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "peptideEvidence_ref"));
        statement.setInt(2, spectrum_identification_item_id);
        statement.executeUpdate();
    }

    private void createDbSequence(XMLStreamReader reader, int sequence_collection_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("createDbSequenceStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "accession"));
        statement.setString(3, reader.getAttributeValue(null, "searchDatabase_ref"));
        statement.setInt(4, sequence_collection_id);
        statement.executeUpdate();
    }

    private void createPeptide(XMLStreamReader reader, int sequence_collection_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createPeptideStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setInt(2, sequence_collection_id);
        int peptide_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("peptidesequence")) {
                    createPeptideSequence(reader, peptide_id);
                } else if (reader.getLocalName().equalsIgnoreCase("modification")) {
                    createModification(reader, peptide_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("peptide")) {
                    return;
                }
            }
        }
    }

    private void createModification(XMLStreamReader reader, int peptide_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createModificationStatement");
        statement.clearParameters();        
        statement.setDouble(1, Double.parseDouble(reader.getAttributeValue(null, "monoisotopicMassDelta")));
        statement.setString(2, reader.getAttributeValue(null, "residues"));
        statement.setInt(3, Integer.parseInt(reader.getAttributeValue(null, "location")));
        statement.setInt(4, peptide_id);
        int modification_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, modification_id, CvParamParent.MODIFICATION);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("modification")) {
                    return;
                }
            }
        }
    }

    private void createPeptideSequence(XMLStreamReader reader, int peptide_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createPeptideSequenceStatement");
        statement.clearParameters();        
        statement.setString(1, reader.getElementText());
        statement.setInt(2, peptide_id);             
        statement.executeUpdate();
    }

    private void createPeptideEvidence(XMLStreamReader reader, int sequence_collection_id) throws SQLException, XMLStreamException {       
        PreparedStatement statement = dmlStatements.get("createPeptideEvidenceStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setBoolean(2, Boolean.parseBoolean(reader.getAttributeValue(null, "isDecoy")));
        statement.setString(3, reader.getAttributeValue(null, "post"));
        statement.setString(4, reader.getAttributeValue(null, "pre"));
        statement.setInt(5, Integer.parseInt(reader.getAttributeValue(null, "end")));
        statement.setInt(6, Integer.parseInt(reader.getAttributeValue(null, "start")));
        statement.setString(7, reader.getAttributeValue(null, "peptide_ref"));
        statement.setString(8, reader.getAttributeValue(null, "dBSequence_ref"));
        statement.setInt(9, sequence_collection_id);
        int peptide_evidence_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("userparam")) {
                    createUserParam(reader, peptide_evidence_id, UserParamParent.PEPTIDE_EVIDENCE);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("peptideevidence")) {
                    return;
                }
            }
        }        
    }

    private void createCv(XMLStreamReader reader, int cv_list_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("createCvStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "uri"));
        statement.setString(3, reader.getAttributeValue(null, "version"));
        statement.setString(4, reader.getAttributeValue(null, "fullName"));
        statement.setInt(5, cv_list_id);
        statement.executeUpdate();
    }

    private void createAnalysisSoftware(XMLStreamReader reader, int as_list_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createAnalysisSoftwareStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "version"));
        statement.setString(3, reader.getAttributeValue(null, "name"));
        statement.setInt(4, as_list_id);
        int as_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("softwarename")) {
                    createSoftwareName(reader, as_id);                    
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("analysissoftware")) {
                    return;
                }
            }
        }        
    }

    private void createSoftwareName(XMLStreamReader reader, int as_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createSoftwareNameStatement");
        statement.clearParameters();
        statement.setInt(1, as_id);
        int software_name_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, software_name_id, CvParamParent.SOFTWARE_NAME);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("softwarename")) {
                    return;
                }
            }
        }
    }

    private void createPerson(XMLStreamReader reader, int audit_collection_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createPersonStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "firstName"));
        statement.setString(3, reader.getAttributeValue(null, "lastName"));
        statement.setInt(4, audit_collection_id);
        int person_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, person_id, CvParamParent.PERSON);                
                } else if (reader.getLocalName().equalsIgnoreCase("affiliation")) {
                    createAffiliation(reader, person_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("person")) {
                    return;
                }
            }
        }       
    }

    private void createAffiliation(XMLStreamReader reader, int person_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("createAffiliationStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "organization_ref"));
        statement.setInt(2, person_id);
        statement.executeUpdate();
    }

    private void createOrganisation(XMLStreamReader reader, int audit_collection_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createOrganisationStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "id"));
        statement.setString(2, reader.getAttributeValue(null, "name"));
        statement.setInt(3, audit_collection_id);        
        int organisation_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, organisation_id, CvParamParent.ORGANISATION);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("organization")) {
                    return;
                }
            }
        }        
    }

    private void createUserParam(XMLStreamReader reader, int parent_id, UserParamParent parentType) throws SQLException {
        PreparedStatement statement = null;
        switch (parentType) {
            case PEPTIDE_EVIDENCE:
                statement = dmlStatements.get("createUserParamForPeptideEvidenceStatement");
                break;
            case SPECTRUM_IDENTIFICATION_ITEM:
                statement = dmlStatements.get("createUserParamForSpectrumIdentificationItemStatement");
                break;
        }

        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "name"));
        statement.setString(2, reader.getAttributeValue(null, "value"));
        statement.setInt(3, parent_id);
        statement.executeUpdate();
    }

    private CvParam createCvParam(XMLStreamReader reader, int parent_id, CvParamParent parentType) throws SQLException {
        PreparedStatement statement = null;
        switch (parentType) {
            case ROLE:
                statement = dmlStatements.get("createCvParamForRoleStatement");
                break;
            case SOFTWARE_NAME:
                statement = dmlStatements.get("createCvParamForSoftwareNameStatement");
                break;
            case PERSON:
                statement = dmlStatements.get("createCvParamForPersonStatement");
                break;
            case ORGANISATION:
                statement = dmlStatements.get("createCvParamForOrganisationStatement");
                break;
            case MODIFICATION:
                statement = dmlStatements.get("createCvParamForModificationStatement");
                break;
            case SPECTRUM_IDENTIFICATION_ITEM:
                statement = dmlStatements.get("createCvParamForSpectrumIdentificationItemStatement");
                break;
            case SPECTRUM_IDENTIFICATION_RESULT:
                statement = dmlStatements.get("createCvParamForSpectrumIdentificationResultStatement");
                break;
        }
        
        String accession = reader.getAttributeValue(null, "accession");
        String name = reader.getAttributeValue(null, "name");
        String value = reader.getAttributeValue(null, "value");
        String unitAccession = reader.getAttributeValue(null, "unitAccession");
        String unitName = reader.getAttributeValue(null, "unitName");

        
        statement.clearParameters();
        statement.setString(1, accession);
        statement.setString(2, name);
        statement.setString(3, value);
        statement.setString(4, unitAccession);
        statement.setString(5, unitName);
        statement.setInt(6, parent_id);
        statement.executeUpdate();
        
        return new CvParam() {
            @Override
            public String getAccession() {
                return accession;
            }

            @Override
            public String getValue() {
                return value;
            }

            @Override
            public String getName() {
                return name;
            }

            @Override
            public String getUnitName() {
                return unitName;
            }

            @Override
            public String getUnitAccession() {
                return unitAccession;
            }
        };
    }

    private void createContactRole(XMLStreamReader reader, int provider_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createContactRoleStatement");
        statement.clearParameters();
        statement.setString(1, reader.getAttributeValue(null, "contact_ref"));
        statement.setInt(2, provider_id);
        int contact_role_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("role")) {
                    createRole(reader, contact_role_id);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("contactrole")) {
                    return;
                }
            }
        }       
    }

    private void createRole(XMLStreamReader reader, int contact_role_id) throws SQLException, XMLStreamException {
        PreparedStatement statement = dmlStatements.get("createRoleStatement");
        statement.clearParameters();
        statement.setInt(1, contact_role_id);
        int role_id = executeUpdateAndGetGeneratedId(statement);
        
        while (reader.hasNext()) {
            reader.next();
            if (reader.getEventType() == XMLStreamReader.START_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("cvparam")) {
                    createCvParam(reader, role_id, CvParamParent.ROLE);
                }
            } else if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (reader.getLocalName().equalsIgnoreCase("role")) {
                    return;
                }
            }
        }        
    }
    
    public String getMzidVersion(int mzid_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findMzidVersion");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        ResultSet set = statement.executeQuery();
        while(set.next()) {
            return set.getString(1);
        }
        
        throw new RuntimeException("Error: no mzid data stored with database id: " + mzid_id);
    }

    private static int executeUpdateAndGetGeneratedId(PreparedStatement statement) throws SQLException {
        statement.executeUpdate();
        try (ResultSet keyRs = statement.getGeneratedKeys()) {
            if (keyRs.next()) {
                return keyRs.getInt(1);
            } else {
                throw new RuntimeException("Cannot retrieve generated key.");
            }
        }
    }
    
    public AnalysisData getAnalysisData(int mzid_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findAnalysisData");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new AnalysisDataRelational(set.getInt(1));
        }
        
        return null;        
    }

    public List<SpectrumIdentificationList> getSpectrumIdentificationLists(int analysis_data_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findSpectrumIdentificationLists");
        statement.clearParameters();
        statement.setInt(1, analysis_data_id);
        ResultSet set = statement.executeQuery();
        List<SpectrumIdentificationList> silList = new LinkedList<>();
        while (set.next()) {
            silList.add(new SpectrumIdentificationListRelational(set.getInt(1), set.getString(2)));
        }
        
        return silList;
    }
    
    public List<ProteinDetectionList> getProteinDetectionLists(int analysis_data_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findProteinDetectionLists");
        statement.clearParameters();
        statement.setInt(1, analysis_data_id);
        ResultSet set = statement.executeQuery();
        List<ProteinDetectionList> pdList = new LinkedList<>();
        while (set.next()) {
            pdList.add(new ProteinDetectionListRelational(set.getInt(1), set.getString(2)));
        }
        
        return pdList;
    }
    
    public AnalysisProtocolCollection getAnalysisProtocolCollection(int mzid_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findAnalysisProtocolCollection");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        ResultSet set = statement.executeQuery();
         if (set.next()) {
             return new AnalysisProtocolCollectionRelational(set.getInt(1));
         }
         
         return null;
    }
    
    public SequenceCollection getSequenceCollection(int mzid_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findSequenceCollection");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new SequenceCollectionRelational(set.getInt(1));
        }
        
        return null;
    }
    
    public PeptideEvidence findPeptideEvidenceByRef(int mzid_id, String ref) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findPeptideEvidenceByRef");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        statement.setString(2, ref);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new PeptideEvidenceRelational(set.getInt(1),set.getString(2),set.getBoolean(3),set.getString(4),set.getString(5),set.getInt(6),set.getInt(7),set.getString(8),set.getString(9));
        }
        
        return null;
    }
    
    public Peptide findPeptideByRef(int mzid_id, String ref) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findPeptideByRef");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        statement.setString(2, ref);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new PeptideRelational(set.getInt(1), set.getString(2));
        }
        
        return null;
    }
    
    public SpectrumIdentificationResult findSpectrumIdentificationResultByRef(int mzid_id, String ref) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findSpectrumIdentificationResultByRef");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        statement.setString(2, ref);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new SpectrumIdentificationResultRelational(set.getInt(1), set.getString(2), set.getString(3));
        }
        
        return null;
    }
    
    public SpectrumIdentificationItem findSpectrumIdentificationItemByRef(int mzid_id, String ref) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findSpectrumIdentificationItemByRef");
        statement.clearParameters();
        statement.setInt(1, mzid_id);
        statement.setString(2, ref);
        ResultSet set = statement.executeQuery();
        if (set.next()) {
            return new SpectrumIdentificationItemRelational(set.getInt(1),set.getString(2),set.getString(3),set.getBoolean(4),set.getInt(5),set.getString(6),set.getDouble(7),set.getDouble(8),set.getInt(9));
        }
        
        return null;
    }
    
    private List<SpectrumIdentificationResult> getSpectrumIdentificationResults(int spectrum_identification_list_id) throws SQLException {
        PreparedStatement statement = dmlStatements.get("findSpectrumIdentificationResults");
        statement.clearParameters();
        statement.setInt(1, spectrum_identification_list_id);
        ResultSet set = statement.executeQuery();
        List<SpectrumIdentificationResult> sirList = new ArrayList<>();
        while (set.next()) {
            sirList.add(new SpectrumIdentificationResultRelational(set.getInt(1), set.getString(2), set.getString(3)));
        }
        
        return sirList;
    }

    private enum CvParamParent {

        ROLE, SOFTWARE_NAME, PERSON, ORGANISATION, MODIFICATION, SPECTRUM_IDENTIFICATION_ITEM, SPECTRUM_IDENTIFICATION_RESULT
    }

    private enum UserParamParent {

        PEPTIDE_EVIDENCE, SPECTRUM_IDENTIFICATION_ITEM
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.liv.pgb.psi.utils.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SPerkins
 */
public class Config {
    private DatabaseEngine database_engine;
    private String database_host;
    private int database_port;
    private String database_user;
    private String database_pass;
    private static Config INSTANCE = new Config();
    
    private Config() {
        readConfig();    
    }
    
    public static Config getInstance() {
        return INSTANCE;
    }
    
    public DatabaseEngine getDatabaseEngine() {
        return this.database_engine;
    }
    
    public String getDatabaseHost() {
        return this.database_host;
    }
    
    public int getDatabasePort() {
        return this.database_port;
    }
    
    public String getDatabaseUser() {
        return this.database_user;
    }
    
    public String getDatabasePass() {
        return this.database_pass;
    }
    
    private void readConfig() {
        URL resource = getClass().getClassLoader().getResource("psi-core.config");        
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.openStream()))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("=")) {
                    String[] split = line.split("=");
                    readKeyValue(split[0].trim(), split[1].trim());
                }
            }
        
        } catch (IOException ex) {
            Logger.getLogger(Config.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    private void readKeyValue(String key, String value) {
        switch (key.toUpperCase()) {
            case "DATABASE_ENGINE":
                database_engine = DatabaseEngine.valueOf(value);
                break;
            case "DATABASE_HOST":
                database_host = value;
                break;
            case "DATABASE_PORT":
                database_port = Integer.parseInt(value);
                break;
            case "DATABASE_USER":
                database_user = value;
                break;
            case "DATABASE_PASS":
                database_pass = value;
                break;
        }
    }
    
    public enum DatabaseEngine {
        HSQLDB,
        SQLITE,
        MYSQL
    }
}
